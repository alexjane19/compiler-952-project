reserved = ('INPUT', 'PRINT', 'ELSE', 'WHILE', 'IF' ,'STR', 'LEN' , 'CHOP'
            )

tokens = reserved + (
    'ID',
    'ASSIGN', 'NUMBER', 'NUMBER_FLOAT', 'TEXT',
    'LPAR', 'RPAR', 'RBRACKET','LBRACKET', 'LBRACE', 'RBRACE',
    'EQUAL', 'UNEQUAL', 'LARGER', 'LESS',
    'PLUS','MINUS','TIMES','DIVIDE','POW',
    'SEMICOLON', 'COLON', 'CAMMA',
    'TEXT_COMMENT'
    # , 'NLINE'
)


t_ASSIGN = r':='
t_EQUAL         = r'='
t_UNEQUAL       = r'<>'
t_LARGER        = r'>'
t_LESS          = r'<'
t_PLUS          = r'\+'
t_MINUS         = r'-'
t_TIMES         = r'\*'
t_DIVIDE        = r'/'
t_POW  = r'\^'
t_LPAR          = r'\('
t_RPAR          = r'\)'
t_LBRACKET = '\['
t_RBRACKET = '\]'
t_LBRACE = '\{'
t_RBRACE = '\}'
t_CAMMA = r','
t_SEMICOLON = r';'
t_COLON = r':'
t_ignore        = " \t\n"



def t_NUMBER_FLOAT(t):
    r'\d+(\.\d+)'
    try:
        t.value = float(t.value)
    except ValueError:
        print("Integer value too large %d", t.value)
        t.value = 0
    return t


def t_NUMBER(t):
    r'\d+'
    try:
        t.value = int(t.value)
    except ValueError:
        print("Integer value too large %d", t.value)
        t.value = 0
    return t


def t_TEXT_COMMENT(t):
    '''\/\*([^(\/\*)(\*\/)])+\*\/'''
    return t

def t_TEXT(t):
    r'\".*\"'
    t.value = t.value[1:-1]
    return t








# def t_NLINE(t):
#     r'\n'
#     t.lexer.lineno += 1
#     return t


def t_error(t):
    print("Illegal character '%s'" % t.value[0])
    t.lexer.skip(1)

reserved_map = {}
for r in reserved:
    reserved_map[r.lower()] = r


def t_ID(t):
    r'[_a-zA-Z][_a-zA-Z\d]*'
    t.type = reserved_map.get(t.value,"ID")
    return t

# Build the lexer
import ply.lex as lex
lexer = lex.lex()

id_table = {}
tmpID = 0;
lID = 0

def gettmp():
    global tmpID
    tmpID += 1
    return 'var'+str(tmpID-1)


def getlabel():
    global lID
    lID += 1
    return 'label'+str(lID-1)

def p_Start(t):
    'start : statements'
    out = ''
    out += t[1][0]
    out += 'hlt' + '\n'
    for x in id_table:
        if (id_table[x][0] == ''):
            tmp = 0
        else:
            tmp = id_table[x][0]
        out += str(id_table[x][1]) + ' db ' + str(tmp) + '\n'
    try:
        output_file = open('output.asm', 'w')
    except (FileNotFoundError, IOError):
        print('ERROR in open output file')
    output_file.write(out)
    output_file.close()
    print(out)

def p_statements(t):
    '''statements : comment statements
         | comment
         | statement
         | statement comment
    '''


def p_comment(t):
    '''comment : TEXT_COMMENT'''

def p_statement(t):
    '''statement : LBRACE state RBRACE
    '''
    t[0] = {}
    t[0][0] = t[2][0]



def p_state(t):
    '''state : assign_statement state
        | if_statement state
        | while_statement state
        | assign_statement
        | if_statement
        | while_statement
    '''
    t[0] = {}
    if(len(t) == 3):
        if(type(t[1])== dict): t[0][0] = t[1][0] + t[2][0]
        else: t[0][0] = t[2][0]
    elif(len(t) == 2):
        if(type(t[1]) == dict): t[0][0] = t[1][0]

def p_state_1(t):
    '''state : comment state
            | comment
        '''
def p_assign_statement(t):
    '''assign_statement : ID ASSIGN E SEMICOLON
        | ID ASSIGN INPUT LPAR RPAR SEMICOLON
        | ID ASSIGN INPUT LPAR TEXT RPAR SEMICOLON
    '''
    if len(t) == 5:
        t[0] = {}
        if (t[1] in id_table):
            t[0][0] = t[3][0] + 'mov ' + id_table[t[1]][1] + ',' + t[3][1] + '\n'
        else:
            print('ERROR :: ' + t[1] + ' not defined')

    elif len(t) == 7:
        id_table[t[1]] = {}
        id_table[t[1]][0] = ''
        id_table[t[1]][1] = gettmp()
        t[0] = {}
        id_table[t[1]][0] = ''
        id_table[t[1]][1] = gettmp()
        t[0][0] = 'inp ' + id_table[t[1]][1] + '\n'
    elif len(t) == 8:

        id_table[t[1]] = {}
        id_table[t[1]][0] = ''
        id_table[t[1]][1] = gettmp()
        t[0] = {}
        tmp = gettmp()
        id_table[tmp] = {}
        id_table[tmp][0] = t[5]
        id_table[tmp][1] = tmp
        t[0][0] = 'inp ' + 'p(' + tmp +') ' + id_table[t[1]][1]+ '\n'


        # print(t[1], t[3])

def p_assign_statement_1(t):
    '''assign_statement : PRINT LPAR TEXT RPAR SEMICOLON
    '''
    t[0] = {}
    tmp = gettmp()
    id_table[tmp] = {}
    id_table[tmp][0] = t[3]
    id_table[tmp][1] = tmp
    t[0][0] = 'print ' + tmp + '\n'


def p_assign_statement_2(t):
    '''assign_statement : PRINT LPAR ID RPAR SEMICOLON
    '''
    t[0] = {}
    t[0][0] = 'print ' + id_table[t[3]][1] + '\n'

def p_assign_statement_3(t):
    '''assign_statement : PRINT LPAR NUMBER RPAR SEMICOLON
        | PRINT LPAR NUMBER_FLOAT RPAR SEMICOLON
    '''
    t[0] = {}
    tmp = gettmp()
    id_table[tmp] = {}
    id_table[tmp][0] = t[3]
    id_table[tmp][1] = tmp
    t[0][0] = 'print ' + tmp + '\n'


def p_assign_statement_4(t):
    '''assign_statement : PRINT LPAR TEXT CAMMA ID RPAR SEMICOLON
    '''
    t[0] = {}
    tmp = gettmp()
    id_table[tmp] = {}
    id_table[tmp][0] = t[3]
    id_table[tmp][1] = tmp
    t[0][0] = 'print ' + tmp + " + " + id_table[t[5]][1] + '\n'

def p_assign_statement_5(t):
    '''assign_statement : PRINT LPAR E RPAR SEMICOLON
    '''
    t[0] = {}
    t[0][0] = t[3][0] + 'print ' + t[3][1] + '\n'

def p_if_statement(t):
    '''if_statement : IF LPAR E LESS E RPAR statement
        | IF LPAR E LESS E RPAR statement ELSE statement
        | IF LPAR E LARGER E RPAR statement
        | IF LPAR E LARGER E RPAR statement ELSE statement
        | IF LPAR E UNEQUAL E RPAR statement
        | IF LPAR E UNEQUAL E RPAR statement ELSE statement
        | IF LPAR E EQUAL E RPAR statement
        | IF LPAR E EQUAL E RPAR statement ELSE statement'''

def p_while_statement(t):
    '''while_statement : WHILE LPAR E LESS E RPAR statement
        | WHILE LPAR E LESS E RPAR statement ELSE statement
        | WHILE LPAR E LARGER E RPAR statement
        | WHILE LPAR E LARGER E RPAR statement ELSE statement
        | WHILE LPAR E UNEQUAL E RPAR statement
        | WHILE LPAR E UNEQUAL E RPAR statement ELSE statement
        | WHILE LPAR E EQUAL E RPAR statement
        | WHILE LPAR E EQUAL E RPAR statement ELSE statement
'''
    l1 = getlabel()
    l2 = getlabel()
    l3 = getlabel()
    a = t[3][1]
    b = t[5][1]
    operator = t[4]
    code = ''

    code += 'cmp ' + a + ',' + b + '\n'

    if (operator == '='):
        code += 'jne ' + l2 + '\n'
    elif (operator == '<>'):
        code += 'je ' + l2 + '\n'
    elif (operator == '>'):
        code += 'jbe ' + l2 + '\n'
    elif (operator == '<'):
        code += 'jae ' + l2 + '\n'

    code += l1 + ': '
    code += 'cmp ' + a + ',' + b + '\n'

    if (operator == '='):
        code += 'jne ' + l3 + '\n'
    elif (operator == '<>'):
        code += 'je ' + l3 + '\n'
    elif (operator == '>'):
        code += 'jbe ' + l3 + '\n'
    elif (operator == '<'):
        code += 'jae ' + l3 + '\n'

    code += t[7][0]
    code += 'jmp ' + l1 + '\n'
    code += l2 + ': '
    if (len(t) == 10): code += t[9][0] + l3 + ': '

    t[0] = {}
    t[0][0] = code


def p_E(t):
    '''E : expression'''
    t[0] = {}
    t[0][0] = t[1][0]
    t[0][1] = t[1][1]


def p_E_1(t):
    '''E : LEN LPAR ID RPAR'''

def p_E_2(t):
    '''E : STR LPAR ID RPAR'''

def p_E_3(t):
    '''E : CHOP LPAR ID RPAR'''

def p_E_4(t):
    '''E : ID LBRACKET NUMBER RBRACKET
        | ID LBRACKET NUMBER COLON NUMBER RBRACKET
        | ID LBRACKET COLON NUMBER RBRACKET
        | ID LBRACKET NUMBER COLON RBRACKET'''





def p_expression(t):
    '''expression : expression PLUS expression
                  | expression MINUS expression
                  | expression TIMES expression
                  | expression DIVIDE expression
                  | expression POW expression'''
    t[0] = {}
    tmp = gettmp()
    id_table[tmp] = {}
    id_table[tmp][0] = ''
    id_table[tmp][1] = tmp
    t[0][1] = tmp
    if t[2] == '+'  : t[0][0] = t[1][0] + t[3][0] + 'add ' + tmp + ',' + t[1][1] + ',' + t[3][1] + '\n'
    elif t[2] == '-': t[0][0] = t[1][0] + t[3][0] + 'sub ' + tmp + ',' + t[1][1] + ',' + t[3][1] + '\n'
    elif t[2] == '*': t[0][0] = t[1][0] + t[3][0] + 'mul ' + tmp + ',' + t[1][1] + ',' + t[3][1] + '\n'
    elif t[2] == '/': t[0][0] = t[1][0] + t[3][0] + 'div ' + tmp + ',' + t[1][1] + ',' + t[3][1] + '\n'
    elif t[2] == '^': t[0][0] = t[1][0] + t[3][0] + 'pow ' + tmp + ',' + t[1][1] + ',' + t[3][1] + '\n'


def p_expression_group(t):
    'expression : LPAR expression RPAR'
    t[0] = {}
    t[0][0] = t[2][0]
    t[0][1] = t[2][1]

def p_expression_number(t):
    'expression : NUMBER'
    print("p_expression_number: " ,t[1])
    t[0] = {}
    tmp = gettmp()
    id_table[tmp] = {}
    id_table[tmp][0] = int(t[1])
    id_table[tmp][1] = tmp
    t[0][1] = tmp
    t[0][0] = ''

def p_expression_number_float(t):
    'expression : NUMBER_FLOAT'
    print("p_expression_number_float: " ,t[1])
    t[0] = {}
    tmp = gettmp()
    id_table[tmp] = {}
    id_table[tmp][0] = float(t[1])
    id_table[tmp][1] = tmp
    t[0][1] = tmp
    t[0][0] = ''



def p_expression_name(t):
    'expression : ID'
    t[0] = {}
    t[0][0] = ''
    t[0][1] = ''
    if(t[1] in id_table):
        t[0][1] = id_table[t[1]][1]
    else: print('ERROR : ' + t[1] + ' not defined')
    # print("p_expression_name: " ,t[1])



def p_expression_name_str(t):
    'expression : TEXT'
    t[0] = {}
    t[0][0] = ''
    t[0][1] = ''
    if(t[1] in id_table):
        t[0][1] = id_table[t[1]][1]
    else: print('ERROR : ' + t[1] + ' not defined')

def p_error(t):
    print("Syntax error at '%s'" % t.value)


test = '''
/* salas */
{
b:="qweqojwe";
  a:=input("Enter a "); /* تابع مانند python است */
  t:=input(); /* می‌تواند رشته‌ای برای نمایش نیز نداشته باشد  */
  s:=1;
    s:=1.2;

  if(a<b){
    s:=a+b*(2+1+a);
  }
  else{
    s:= b+a*(2+1+b);
    x:= a[1];
  }
  while(a>0){
    print(a);
    a := a-1;
  }
  else{
   a:=0;
  }
  print(s);
  g:=input("Enter g>0 ");
}
/* salas */
'''


# lexer.input(test)
# while True:
#     tok = lex.token()
#     print(tok)
#     if not tok: break

import ply.yacc as yacc
parser = yacc.yacc()
parser.parse(test)




